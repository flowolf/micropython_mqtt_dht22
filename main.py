import dht
import machine
import time
import network
import socket
import ubinascii
import json
import math
#import bmp180
#from umqtt.simple import MQTTClient
from umqtt.robust import MQTTClient



debug = True
debug_pin = 2
NETWORK_CONNECT_TIMEOUT = 60 #seconds
DEV_ID = "uPy-{0}".format(ubinascii.hexlify(machine.unique_id()).decode('utf-8'))
MY_NETS = {
            #"your_net": "password",
          }

INTERVAL = 60
deep_sleep = False
# configure RTC.ALARM0 to be able to wake the device
rtc = machine.RTC()
rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)

# check if the device woke from a deep sleep
if machine.reset_cause() == machine.DEEPSLEEP_RESET:
    print('woke from a deep sleep')
    pass
else:
    print("sleep at the beginning...")
    time.sleep(10)

# set RTC.ALARM0 to fire after 10 seconds (waking the device)
rtc.alarm(rtc.ALARM0, INTERVAL*1000)

d = dht.DHT22(machine.Pin(5))

led = machine.Pin(2,machine.Pin.OUT)
led.value(0)
mqtt_server = "192.168.1.1"
topic = "/sensor/uPy/"+ DEV_ID +"/temp"

mqtt_user = "mymqttuser"
mqtt_pw = "mymqttpassword"


start_time = time.ticks_ms()

#adc = machine.ADC(0)
wlan = network.WLAN(network.STA_IF)
def do_connect():
    print("getting network")
    network_connect_start_time = time.ticks_ms()
    import network

    # disable AP wifi
    ap = network.WLAN(network.AP_IF)
    ap.active(False)

    if not wlan.active():
        wlan.active(True)
    nets = wlan.scan()
    for net in nets:
        net = net[0].decode('utf-8')
        if net in MY_NETS and not wlan.isconnected():
            print('connecting to network: {}'.format(net))
            wlan.connect(net, MY_NETS[net])#, timeout=5000)
            while not wlan.isconnected():
                if debug:
                    led.value(1) # off
                if wlan.status() == network.STAT_IDLE:
                    print("ERROR: nothing going on")
                    break
                # if wlan.status() == network.STAT_CONNECTING:
                #     print("INFO: connecting to network")
                if wlan.status() == network.STAT_GOT_IP:
                    print("INFO: what are you doing here, shouldn't be here...")
                if wlan.status() == network.STAT_WRONG_PASSWORD or\
                    wlan.status() == network.STAT_NO_AP_FOUND or\
                    wlan.status() == network.STAT_CONNECT_FAIL:

                    print("ERROR: wifi has issues ({})".format(wlan.status()))
                    break
                if (network_connect_start_time + NETWORK_CONNECT_TIMEOUT*1000) < time.ticks_ms():
                    print("ERROR: network timeout. trying other network, or sleeping")
                    break
                machine.idle()
    if debug:
        led.value(0) # on
    print('network config:', wlan.ifconfig())

c = MQTTClient(DEV_ID, mqtt_server, user=mqtt_user, password=mqtt_pw)

while True:
    current_time = time.ticks_ms()
    led.value(0)
    do_connect()
    try:
        d.measure()
    except Exception as e:
        print(e)
        #time.sleep(1)
        continue
    #time.sleep(0.5)
    temp = math.ceil(d.temperature()*10-0.5)/10.
    humi = math.ceil(d.humidity()*10-0.5)/10.
    datat = json.dumps({
        b"id": DEV_ID,
        b"temperature": temp,
        b"time": current_time
    })
    datah = json.dumps({
        b"id": DEV_ID,
        b"humidity": humi,
        b"time": current_time
    })
    print(datat)
    print(datah)

    # do not send weird data
    if temp == humi and temp == 0.0:
        print("not good data, trying again")
        time.sleep(3)
        continue

    #ap.ifconfig()
    try:
        c.connect()
        print("publishing: ")

        print(topic + "/temp: {}".format(datat))
        c.publish(topic + "/temp",datat, retain=True)

        print(topic + "/humi: {}".format(datah))
        c.publish(topic + "/humi",datah, retain=True)
        print('sent')
        c.disconnect()
    except:
        print("ERROR: connecting or sending data to MQTT server!")

    led.value(1)
    if deep_sleep:
    # put the device to sleep
        machine.deepsleep()
    else:
        time.sleep(INTERVAL)
